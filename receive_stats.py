#!/usr/bin/env python3
import getopt
import os
import socket
import sys
import netifaces


def get_ips():
    my_ips = list()
    interfaces = netifaces.interfaces()
    for i in interfaces:
        if i == 'lo':
            continue
        iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
        if iface is not None:
            for j in iface:
                ip = j['addr']
                my_ips.append(ip)
    return my_ips


def receive(ip, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((ip, port))
        s.listen(10)
        while True:
            conn, addr = s.accept()
            buf = conn.recv(64)
            if len(buf) > 0:
                print(buf)


def usage():
    print('{} [--ip=<ip address>] [--port=<port>]'.format(os.path.basename(__file__)))
    exit(0)


def main(argv):
    listen_port = 4444
    my_ip = None

    try:
        opts, args = getopt.getopt(argv, "hp:i:", ['help', 'ip=', 'port='])
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                usage()
            elif opt in ('-i', '--ip'):
                my_ip = arg
            elif opt in ('-p', '--port'):
                listen_port = int(arg)
    except getopt.GetoptError as e:
        print(e.msg)
        usage()

    # If no IP provided, guess that the first IP found is okay to listen on
    if not my_ip:
        all_ips = get_ips()
        my_ip = all_ips[0]

    print("listening at {} on port {}".format(my_ip, listen_port))

    receive(my_ip, listen_port)


if __name__ == "__main__":
    main(sys.argv[1:])
