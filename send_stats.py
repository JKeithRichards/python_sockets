#!/usr/bin/env python3
import getopt
import os
import socket
import sys
import time

import psutil


def send(hostname, port, content):
    print("{}:{}: '{}'".format(hostname, port, content).replace('\r', '').replace('\n', ''))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((hostname, port))
    s.sendall(str.encode(content))
    s.shutdown(socket.SHUT_WR)
    s.close()


def usage():
    print('{} --name=<name> [--ip=<ip address>] [--port=<port>]'.format(os.path.basename(__file__)))
    exit(0)


def main(argv):
    graphite_ip = "192.168.1.17"
    graphite_port = 2003
    my_name = None
    send_interval = 60

    try:
        opts, args = getopt.getopt(
            argv, "h:n:i:p:s:",
            ['help', 'name=', 'ip=', 'port=', 'sleep='])
    except getopt.GetoptError as e:
        print(e.msg)
        usage()

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
        elif opt in ('-n', '--name'):
            my_name = arg
        elif opt in ('-i', '--ip'):
            graphite_ip = arg
        elif opt in ('-p', '--port'):
            graphite_port = int(arg)
        elif opt in ('-s', '--sleep'):
            send_interval = int(arg)
        else:
            usage()

    if not my_name:
        print("Please provide a name")
        usage()

    print("name = {}, ip = {}, port = {}".format(my_name, graphite_ip, graphite_port))
    print("Sending every {} seconds.".format(send_interval))

    while True:
        time_now = int(time.time())

        cpu_message = "{}.cpu {} {}\n".format(
            my_name,
            psutil.cpu_percent(interval=1),
            time_now)
        send(graphite_ip, graphite_port, cpu_message)

        mem_message = "{}.ram {} {}\n".format(
            my_name,
            psutil.virtual_memory()[3],
            time_now)
        send(graphite_ip, graphite_port, mem_message)

        # Network bytes sent on all interfaces
        net_sent = psutil.net_io_counters(pernic=True)
        for interface in net_sent:
            bytes_sent = "{}.{}.sent {} {}\n".format(
                my_name,
                str(interface).replace(' ', '-'),
                str(psutil.net_io_counters().bytes_sent).replace(' ', '_'),
                time_now)
            send(graphite_ip, graphite_port, bytes_sent)

            bytes_recv = "{}.{}.recv {} {}\n".format(
                my_name,
                str(interface).replace(' ', '-'),
                str(psutil.net_io_counters().bytes_recv).replace(' ', '_'),
                time_now)
            send(graphite_ip, graphite_port, bytes_recv)

        print("")
        time.sleep(send_interval)


if __name__ == "__main__":
    main(sys.argv[1:])
