#!/usr/bin/env python

import netifaces

interfaces = netifaces.interfaces()
for i in interfaces:
    if i == 'lo':
        continue
    iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
    if iface is not None:
        for j in iface:
            print(j['addr'])
